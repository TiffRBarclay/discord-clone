import { Avatar } from '@material-ui/core'
import React from 'react'
import './ChatMessage.css'

function ChatMessage() {
    return (
        <div className="chat-message">
            <Avatar />
            <div className="chat-message-info">
                <h4>Tiffany Barclay
                    <span className="chat-message-timestamp">
                        This is a timestamp
                    </span>
                </h4>
                <p>This is a message</p>
            </div>
        </div>
    )
}

export default ChatMessage
