import React from 'react'
import './Sidebar.css'

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import SignalCellularAltIcon from '@material-ui/icons/SignalCellularAlt';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import CallIcon from '@material-ui/icons/Call';
import MicIcon from '@material-ui/icons/Mic';
import HeadsetIcon from '@material-ui/icons/Headset';
import SettingsIcon from '@material-ui/icons/Settings';

import SidebarChannel from './SidebarChannel';
import { Avatar } from '@material-ui/core';

function Sidebar() {
    return (
        <div className="sidebar">
            <div className="sidebar-top">
                <h3>Test Server</h3>
                <ExpandMoreIcon />
            </div>
            <div className="sidebar-channels">
                <div className="sidebar-channels-header">
                    <div className="sidebar-header">
                        <ExpandMoreIcon />
                        <h4>Text Channels</h4>
                    </div>
                    <AddIcon className="sidebar-addChannel" />
                </div>
                <div className="sidebar-channels-list">
                    <SidebarChannel />
                    <SidebarChannel />
                    <SidebarChannel />
                    <SidebarChannel />
                </div>
            </div>
            <div className="sidebar-voice">
                <SignalCellularAltIcon 
                    className="sidebar-voice-icon" 
                    fontSize="large"
                />
                <div className="sidebar-voice-info">
                    <h3>Voice Connected</h3>
                    <p className="sidebar-voice-info-p">Stream</p>
                </div>
                <div className="sidebar-voice-icons">
                    <InfoOutlinedIcon />
                    <CallIcon />
                </div>
            </div>
            <div className="sidebar-profile">
                <Avatar src="https://lh3.googleusercontent.com/-EjoS2ug7qdo/X1cSSXI1O2I/AAAAAAAAEi4/ibUj9vgazvofJ_PQQQzBMj5lKsJqDqKYQCEwYBhgLKtQDAL1OcqyFjkM_e_ila2qWEsklcTXkv4VS7TI1fEvAWW6XR3XlXUi5Fh4tFEeQmTJ5Ka4kDx-H49S_OoUuTu_c7OsoGKbDwh6cHvKKQOm6CptsSv-hKzRvXN_d40hC6frDu_uLV0E-2Ig7n0qn0njpgpG3fZYvrnqVDuBUQeAtz4_Hc6PoXpUsU8EFKYoLACBgsZGSILmV7fHn7h_8WKK6NePzEk8_ybtND1oWZacrI9QYLea-4l7PW87ooArLE-u79dU2l8V5CxLehBrrPriNZ7HIEwUy57vJJi9PjDpok7Ysw-Uvpz1lfo0Sssv13k2a3OHUs_debob9k6NJArXbX1TNnphwCP8KvLXn1zTdaHweJ0tr_sg-uvcCwxlTZbUYAgazxXyYXAfzRM5_cHayZ7tHMdb1IYtNo9VMyO_ER_3v6w4dHBPp7RwFsfeGFetfIbWagQsmJpB-5RXXko2DOMEwBXSurpmuU0QX3W6y0Hi5zbXijCDnbi_sFlblIIBXAQPmlow_h8MYtRwu8wv1m2VvbzlvrLz9C2ugEzPSk2QyYpuKn2PjcjQNE7yvPw0xSmBu_c5cxE8IaMFtbuqwUR67znWHRlbwgPysw4RKIVEM5DgcMLz_4vwF/w139-h140-p/Photo.jpg"/>
                <div className="sidebar-profile-info">
                    <h3>Tiffany Barclay</h3>
                    <p className="sidebar-profile-info-p">#ThisIsMyId</p>
                </div>
                <div className="sidebar-profile-icons">
                    <MicIcon />
                    <HeadsetIcon />
                    <SettingsIcon />
                </div>
            </div>
        </div>
    )
}

export default Sidebar
