import React from 'react'
import './Chat.css';
import ChatHeader from './ChatHeader';
import ChatMessage from './ChatMessage';

import AddCircleIcon from '@material-ui/icons/AddCircle'
import CardGiftcardIcon from '@material-ui/icons/CardGiftcard'
import GifIcon from '@material-ui/icons/Gif'
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions'

function Chat() {
    return (
        <div className="chat">
            <ChatHeader />

            <div className="chat-messages">
                <ChatMessage />
                <ChatMessage />
                <ChatMessage />
            </div>
            <div className="chat-input">
                <AddCircleIcon 
                    fontSize="large" 
                />
                <form>
                    <input placeholder="Message #YouTube" />
                    <button type="submit" className="chat-input-button">
                        Send Message
                    </button>
                </form>
                <div className="chat-input-icons">
                    <CardGiftcardIcon fontSize="large"/>
                    <GifIcon fontSize="large"/>
                    <EmojiEmotionsIcon fontSize="large"/>
                </div>
            </div>
        </div>
    )
}

export default Chat
