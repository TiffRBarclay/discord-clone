import React from 'react'
import './SidebarChannel.css'

function SidebarChannel({id, channel}) {
    return (
        <div className="sidebar-channel">
            <h4><span className="sidebar-channel-hash">#</span>YouTube</h4>
        </div>
    )
}

export default SidebarChannel
